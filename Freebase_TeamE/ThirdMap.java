/*
 * Filename: ThirdMap.java
 * Description: The third map does the simple task of outputting the second reducer's output as key value pairs.
 * 
 * Author: Sakethram Karumuri
 * 
 * Input format:
 * Key: Person
 * Value: Person-Relationship
 * 
 * Output format:
 * Key: Person
 * Value: Person-Relationship
 * 
 * Example:
 * I/P:
 * m.0jcx		m.01rymn-person.spouse_s: m.04hjsn-person.sibling_s
 * 
 * O/P:
 * m.0jcx		m.01rymn-person.spouse_s: m.04hjsn-person.sibling_s
 * 
 *  
 */
package Freebase_TeamE;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class ThirdMap 
	extends  Mapper<Object,Text,Text,Text>
{
	
	public void map(Object key,Text value,Context context) throws IOException,InterruptedException
	{
		//Convert the input value into string
		String line = value.toString();
		
		//Split the value string using \t as the delimiter, which will result in to strings:
		//Split[0] has person1
		//Split[1] has the adjacent person2 of person1 and the adjacent persons of person2
		String[] split = line.split("\t");
		
		//Write the output to the context object
		context.write(new Text(split[0]), new Text(split[1]));
	}
}
