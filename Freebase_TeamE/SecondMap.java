/*
 * Filename: SecondMap.java
 * Description: This is the map for the second job.  It takes the value and splits it to get the two people and the relationship between them.  It then takes the first
 * person and makes them the key.  Then it makes the value person2-relationship for ease of acess in the second reduce phase.
 * 
 * Author: Brenda
 * 
 * Input: First Reducers output
 * Output: Same format as First reducer but with the second person in front of the relationship in the value.
 * 
 * Example:
 * I/P: Key:person1		Value:relationship	person2	
 * O/P: Key:person1		Value:person2-relationship
 * 
 */


package Freebase_TeamE;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class SecondMap 
	extends  Mapper<Object,Text,Text,Text>
{
	
	public void map(Object key,Text value,Context context) throws IOException,InterruptedException
	{
		String line = value.toString();
		//split the value by tabs
		String[] split = line.split("\t");
		//switch the second person and the relationship
		context.write(new Text(split[0]), new Text(split[2]+"-"+split[1]));
	}
}

