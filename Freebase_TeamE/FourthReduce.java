/*
 * Filename: FourthReduce.java
 * Author: Ravikanth Repaka
 * Description:
 * Members:
 * 			maxCliqueMap	-- It is type of HashMap<String,Integer>. 
 * 					This is using to store whole clique information as a key and number of nodes as a value. 
 * 					After storing all incoming unique cliques in this map, we will sort these cliques based on number of nodes and print the maximum clique. 
 *   
 * 			maxUniqueCliques	-- It is type of HashSet<String>
 *					This will store only unique combination of nodes. We will use this information to store only unique clique in maxCliqueMap.  
 * 
 * Methods:
 * 			reduce(Text key,Iterable<Text> values,Context context)
 * 					-- This function will be called for each input key value pair. 
 * 					   Main logic in this function is store unique cliques in maxCliqueMap.   
 * 				
 * 			cleanup(Context context) throws IOException,InterruptedException
 * 					-- This function will be called only once at the end of this Reduce class.
 * 					   By the time it is calling this function, all unique cliques are stored in maxCliqueMap. Now use maxCliqueMap to get maximum clique among all unique cliques. 
 * 						  
 * Example:
 *  
 * Input-> Cliques
 * 									key												|					  value
 * -------------------------------------------------------------------------------------------------------------------------------
 * MID1 MID2 MID3 RelationsBetweenMIDS												|						3
 * MID1 MID2 MID3 RelationsBetweenMIDS(but order of relations is different)			|						3
 * MID4 MID5 MID6 MID7 RelationsBetweenMIDS											|						4
 * 
 * Output->
 * 									key								|							value
 * -------------------------------------------------------------------------------------------------------------------------------
 * 				Clique size: 4										|				MID4 MID5 MID6 MID7 RelationsBetweenMIDS
 * 
 */



package Freebase_TeamE;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;





public class FourthReduce
	extends Reducer<Text,Text,Text,Text>
{
	//To store entire clique information including relationships as a key and number of nodes as a value
    Map<String,Integer> maxCliquesMap = new HashMap<String,Integer>();
    //To store only nodes in each clique.
	Set<String> maxUniqueCliques = new HashSet<String>();

		 //This function will be called for each input key value pair
	public void reduce(Text key,Iterable<Text> values,Context context) throws IOException,InterruptedException
	{
		//store key information as a string in keyNodes
	    String keyNodes = key.toString();
	    
	    //valuesCount as ArrayList to store clique size 
	    List<String> valuesCount = new ArrayList<String>();
	    
	    //for each value in values add the size to valueCount arrayList. Before adding to arrayList, convert the integer into toString.
	    for(Text val:values)
		valuesCount.add(val.toString());
	    
 
	    //Input key is clique that is concatenation of nodes and relations between nodes.
	    //Split the input line  based on tab delimiter and get the node names. Store the node name in String array.
	    String[] splitKeys = keyNodes.split("\t");
	    
	    String onlyNodes="";
	    //Iterate the loop to each node name. concatenate all the node names as store that in one string. 
	    for(int i=0; i<splitKeys.length-1; i++)
		onlyNodes += splitKeys[i];
	    
	    //check the string of nodenames is already exists in maxUniqueCliques.
	    //If that exists do nothing. Else store the string of nodenames in maxUniqueCliques and entire clique, number of nodes in maxCliquesMap as a key, value pair    
	    if(!maxUniqueCliques.contains(onlyNodes)){
	    	//store in maxUniqueCliques
		maxUniqueCliques.add(onlyNodes);
		//key is entire clique and value is number of nodes
		maxCliquesMap.put(keyNodes, Integer.parseInt(valuesCount.get(0)));
	    }
	}
	
	//This function will be at the end of reduce class. By the time this function will be called, maxCliquesMap will have clique information along with number of nodes.  
	public void cleanup(Context context) throws IOException,InterruptedException {
		// We are using valueComparator to sort in descending order  of cliques based on number of nodes. 
	    ValueComparator bvc =  new ValueComparator(maxCliquesMap);
	    //used a tree map to sort the cliques
	    TreeMap<String,Integer> sorted_map = new TreeMap<String,Integer>(bvc);
	    //putting the values from HashMap to TreeMap
	    sorted_map.putAll(maxCliquesMap);
	
	    //To iterate over all cliques get a iterator object
	    Iterator<Map.Entry<String, Integer>> it = sorted_map.entrySet().iterator();
	    //take maxCliqueSize variable and set as 0 
	    int maxCliqueSize = 0;
	    
		//until all cliques are read
	    while(it.hasNext()){
	    	//Store the each iterator in Entry object to access values.
	    	Map.Entry<String, Integer> entrySet = it.next();
	    	//Retrieve the key from entrySet and store that in personsList
	    	String personsList = entrySet.getKey();
	    	//Replace all , and tabs with new lines. This will be useful in the process of getting person names from MIDS. 
	    	personsList = personsList.replaceAll(",", "\n");
	    	personsList = personsList.replaceAll("\t", "\n");
	    	//If the number of nodes is greater than maxCliquesize then write that clique to the context.
	    	if(entrySet.getValue() >= maxCliqueSize) {
	    		maxCliqueSize = entrySet.getValue();
	    		context.write(new Text("Clique size: "+String.valueOf(maxCliqueSize)+"\n"), new Text(personsList) );
	    	}
	    	//else exit the loop. END OF PROGRAM.
	    	else
	    		break;
	    }
	 }
}
