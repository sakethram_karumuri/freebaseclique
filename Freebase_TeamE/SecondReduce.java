/*
 * Filename: SecondReduce.java
 * Description: This is reducer constructs the graph.  Because of the fact that Hadoop uses many different reducers to process data the vertices between one person and
 * all the other people it touches needs to be stored for each person.  For example if the input is p1 as the key and p2-r,p3-r,p4-r as the value then there needs to be
 * three new outputs that take the form p2 as the key with p1:p2-r,p3-r,p4-r as the value.  This way when it is passed on to the next step not only does the person have the
 * people they have relationship with but the people that all the others have relationships with as well.  To explain further the key of p2 will have a value of p1, who is related
 * to p2 in some way, and all the people who are related to p1 in someway as well.
 * 
 * Author: Brenda
 * 
 * Input: Key: person Value: person2-relationship, person3-relationship...
 * Output: Key: person2 Value: person-relationship:person2-relationship, person3-relationship... for all iterations (next key would be person 3 with the same value)
 * 
 * Example:
 * Input:	Key:p1	Value:	p2-r1,p3-r2,p4-r3
 * Output:	Generate new record for every value in the input
 * 			Key:p2	Value:	p1-r1:p2-r1,p3-r2,p4-r3
 * 				p3			p1-r2:p2-r1,p3-r2,p4-r3
 * 				p4			p1-r3:p2-r1,p3-r2,p4-r3
 * 
 */

package Freebase_TeamE;


import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class SecondReduce
	extends Reducer<Text,Text,Text,Text>
{
	 

	public void reduce(Text key,Iterable<Text> values,Context context) throws IOException,InterruptedException
	{
		
		ArrayList<String> inputValues = new ArrayList<String>(); 
		
		//for all of the values in the value
		for (Text val: values)
				//add values to inputValues
				inputValues.add(val.toString());
		
		//for all values in inputValues
		for(int i=0; i< inputValues.size(); i++)
		{
			//create a new key and value
			String newKey,newValue;
			//split inputValues[i] so that the relationship is in split[1] and the person is in split[0]
			String[] split = inputValues.get(i).split("-");
			//make the person in value the new key 
			newKey = split[0];
		
			//create the new value for the new output oldKeyPerson-relationship
			newValue = key.toString() +"-"+ split[1]+ ":";
			//add all other people that the person in the old key was related to into the new value after the colon
			for(int j=0; j< inputValues.size(); j++)
				newValue += inputValues.get(j) + ",";
			
			//write the new key and value for each different person related with the original key
			newValue = newValue.substring(0, newValue.length() - 1);
			context.write(new Text(newKey), new Text(newValue));
		}
		
		
	}
}
