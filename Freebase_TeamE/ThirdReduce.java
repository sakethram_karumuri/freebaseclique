/*
 * Filename: ThirdReduce.java
 * Description: This reducer does the most important task of finding the maximal clique in the sub graph supplied to it. Each record in its input will form a sub graph. A clique can be computed on this sub graph because all the required adjacency lists will be appended in the 2nd reducer. The algorithm used to find the maximal clique is "Tomita" which we found from a paper cited below. It finds the maximal clique on each and every input records of the reducer. All the cliques will be stored in a map to find the max of maximal cliques on each reducer.
 * 
 * Author: Sakethram Karumuri
 * 
 * Input Format:
 * Key: Person1
 * Value: (Adj person2 to person1-relation b/w them):(Adj person3 to person2-relation),(Adj person4 to person2-relation)
 * 
 * Output Format:
 * Key: Person1	Person2	Person3	Person4...
 * (It has the persons in the maximal clique)
 * 
 * Value: Person1-Person2:Relation b/w them, Person2-Person3:Relation b/w them, Person3-Person4:Relation b/w them
 * (It has the list of relations between each pair in the clique)
 * 
 * Example:
 * I/P: 
 * m.0jcx		m.01rymn-person.spouse_s: m.04hjsn-person.sibling_s, m.8uywhh-person.children: 04ywers-person.parent, 01rymn-person.children
 *  
 * O/P:
 * m.ojcx	m.01rymn	m.04hjsn	m.0jcx-m.01rymn:person.spouse,m.0jcx-04hjsn:person.children,m.04hjsn-m.01rymn:person.children,m.01rymn-m.0jcx:person.spouse, m.04hjsn-m.0jcx-person.children, m.01rymn-m.04hjsn-person.children
 * 
 */

package Freebase_TeamE;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ThirdReduce extends Reducer<Text, Text, Text, Text> {

	//This set contains the list of all maximal cliques from the given input. I used HashSet in order to avoid redundant cliques. I made sure this will happen by sorting the clique before inserting into the set.
	Set<ArrayList<String>> maxCliques = new HashSet<ArrayList<String>>();

	//This map holds the relations between two any persons
	Map<String, String> graphRelations2 = new HashMap<String, String>();

	public void reduce(Text key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {
		Map<String, List<String>> graph = new HashMap<String, List<String>>();
		// For example 1 = [2-rel:1-abc,2-xyz,3-mno],[3-rel:4-abc,5-xyz,6-mno]

		for (Text val : values) {
			// value has the entire line 2-rel:1-abc,2-xyz,3-mno
			String value = val.toString(); 
			
			// gives two parts before and after ":" i.e., 2-rel and 1-abc,2-xyz,3-mno
			String[] adjValues = value.split(":"); 
			// splits 2-rel into 2 and rel
			String[] keyValue = adjValues[0].split("-");
			
			if (!graph.containsKey(key.toString()))
				// puts 1 as key and 2 as value
				graph.put(key.toString(), new ArrayList<String>(Arrays.asList(keyValue[0])));  
			else
				// get the key and add the value
				graph.get(key.toString()).add(keyValue[0]);

			// put the person to person relationship in the map
			// key is person1+person2
			// value is relation between person1 and person2
			if (!graphRelations2.containsKey((key.toString()) + keyValue[0])| graphRelations2.containsKey((keyValue[0])	+ key.toString())) {
				// put (person1 + person2, relation) in the map
				// put (person2 + person1, relation) in the map
				graphRelations2.put(key.toString() + keyValue[0], keyValue[1]);
				graphRelations2.put(keyValue[0] + key.toString(), keyValue[1]);
			}

			// split the values using "," as delimiter 
			String[] adjAdjValues = adjValues[1].split(",");

			// split the adjacency list with "-" as the delimiter and store them in eachValue array
			// repeat the same procedure for each and every adjacency list 
			for (int i = 0; i < adjAdjValues.length; i++) {
				String[] eachValue = adjAdjValues[i].split("-");
				
				// check if the graph already has the key
				// person 1 to person 2
				if (!graph.containsKey(keyValue[0]))
					// if it doesn't have the key, create a new arraylist and put it in the graph
					graph.put(keyValue[0], new ArrayList<String>(Arrays.asList(eachValue[0])));
				else if (!graph.get(keyValue[0]).contains(eachValue[0]))
					// if it has the key, retrieve the arraylist and add the value to the arraylist
					graph.get(keyValue[0]).add(eachValue[0]);

				// repeat the above logic for putting the inverse relation
				// person 2 to person 1
				if (!graph.containsKey(eachValue[0]))
					graph.put(eachValue[0], new ArrayList<String>(Arrays.asList(keyValue[0])));
				else if (!graph.get(eachValue[0]).contains(keyValue[0]))
					graph.get(eachValue[0]).add(keyValue[0]);

				// similar logic applies while inserting the relations into the graphRelations map
				if (!graphRelations2.containsKey(eachValue[0] + keyValue[0])| graphRelations2.containsKey(keyValue[0]	+ eachValue[0])) {
					graphRelations2.put(eachValue[0] + keyValue[0],	eachValue[1]);
					graphRelations2.put(keyValue[0] + eachValue[0],	eachValue[1]);

				}

			}

		}

		// call the maximal clique algorithm
		tomita(graph);
	}

	// cleanup function finds the maximum clique from all the input records and writes it to the context object 
	public void cleanup(Context context) throws IOException,
			InterruptedException {

		//finalMaxCliques has the maximum cliques. This is an arraylist to enable sorting
		List<ArrayList<String>> finalMaxCliques = new ArrayList<ArrayList<String>>(maxCliques);

		// sort the cliques based on their size/length
		Collections.sort(finalMaxCliques, new Comparator<ArrayList<String>>() {
			@Override
			public int compare(ArrayList<String> one, ArrayList<String> two) {
			// returns values accordingly based on the arraylist size
				if (one.size() > two.size())
					return -1;
				else if (one.size() < two.size())
					return 1;
				else
					return 0;
			}
		});

		// If the finalMaxCliques array is not empty, write the cliques to the context
		if (!finalMaxCliques.isEmpty()) {
			// store the size of maximum clique
			int maxCliqueSize = finalMaxCliques.get(0).size();
			
			// iterate the arraylist till we write all the maximal cliques
			for (int i = 0; i < finalMaxCliques.size(); i++) {
				// if the clique is smaller than the maximal clique, stop writing
				if (finalMaxCliques.get(i).size() < maxCliqueSize)
					break;
				// else continue
				StringBuilder newKey = new StringBuilder();
				StringBuilder newValue = new StringBuilder();
				// prepare new key by appending all the person nodes
				for (int j = 0; j < finalMaxCliques.get(i).size(); j++)
					newKey.append(finalMaxCliques.get(i).get(j) + "\t");
				// remove the last \t
				newKey.deleteCharAt(newKey.length() - 1);

				// prepare new value by appending all the person to person relations
				for (int k = 0; k < finalMaxCliques.get(i).size(); k++) {
					for (int l = 0; l < finalMaxCliques.get(i).size(); l++) {
						if (k != l)
							newValue.append(finalMaxCliques.get(i).get(k)
									+ "-"
									+ finalMaxCliques.get(i).get(l)
									+ ":"
									+ graphRelations2.get(finalMaxCliques
											.get(i).get(k)
											+ finalMaxCliques.get(i).get(l))
									+ ",");

					}
				}

				// remove the last comma
				newValue.deleteCharAt(newValue.length() - 1);
				// write the new key value pairs to the context
				context.write(new Text(newKey.toString()),
						new Text(newValue.toString()));
			}
		}

	}

	// This function calls the maximal clique algorithm on each row of the adjacency list matrix in the graph
	public void tomita(Map<String, List<String>> graph) {

		// cand has the vertices that needs to be processed
		// fini has the vertices that are already processed
		List<String> k, cand;
		List<String> fini = new ArrayList<String>();

		// this iterator runs on all the rows in the adj matrix
		Iterator<Entry<String, List<String>>> it = graph.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, List<String>> pairs = (Map.Entry<String, List<String>>) it
					.next();

			String key = pairs.getKey();
			List<String> value = pairs.getValue();
			k = new ArrayList<String>(Arrays.asList(key));
			cand = new ArrayList<String>(value);
			// call the algorithm on each row
			findTomita(graph, k, cand, fini);

		}

	}

	// This function finds the maximal cliques given a graph and adds them to the maxCliques hashset
	public void findTomita(Map<String, List<String>> g, List<String> k,
			List<String> cand, List<String> fini) {
		
		// this is the terminating condition for adding maximal cliques
		if (cand.isEmpty() && fini.isEmpty()) {
			// sort the cliques while adding in order to avoid redundancy
			Collections.sort(k);
			maxCliques.add((ArrayList<String>) k);
			return;
		}

		// declare the variables that are required for calculating maximal clique
		// the main approach is to add each node to an existing and find if the clique still forms or not 
		String pivot = "";
		List<String> ext, temp_u, temp_inter;
		
		// temp_u has all the elements from cand and fini
		temp_u = list_union(cand, fini);

		int maximize_intersect = 0;

		// iterate over temp_u
		for (String s : temp_u) {
			temp_inter = list_intersection(cand, g.get(s));

			// choose the pivot accordingly
			if (temp_inter.size() >= maximize_intersect) {
				maximize_intersect = temp_inter.size();
				pivot = s;
			}

		}

		// subtract the adjacent nodes of pivot from cand array to find ext
		ext = list_subtraction(cand, g.get(pivot));

		// iterate over ext and call findTomita algorithm recursively to find the maximal clique
		for (String temp_ext : ext) {

			List<String> k_q, cand_q, fini_q;
			List<String> te = new ArrayList<String>(Arrays.asList(temp_ext));
			k_q = list_union(k, te);
			cand_q = list_intersection(cand, g.get(temp_ext));
			fini_q = list_intersection(fini, g.get(temp_ext));
			// call recursively untill it meets the terminating condition
			findTomita(g, k_q, cand_q, fini_q);
			// update cand, fini and k
			cand = list_subtraction(cand, te);
			fini = list_union(fini, te);
			k = list_subtraction(k, te);

		}
	}

	// this function subtracts list2 from list1 and returns a new list
	public List<String> list_subtraction(List<String> list1, List<String> list2) {

		List<String> list = new ArrayList<String>();
		for (String t : list1) {
			// check if the element in list 2 exists in list 1
			if (!list2.contains(t))
				list.add(t);
		}
		return list;
	}

	// this function unions the two lists 1 and 2
	public List<String> list_union(List<String> list1, List<String> list2) {
		// take a hashset to avoid redundancy
		Set<String> set = new HashSet<String>();

		set.addAll(list1);
		set.addAll(list2);
		// return the new combined set
		return new ArrayList<String>(set);
	}

	// this method finds the common elements from list1 and list2
	public List<String> list_intersection(List<String> list1, List<String> list2) {
		List<String> list = new ArrayList<String>();

		for (String t : list1) {
			// check if both the lists have the elements
			if (list2.contains(t)) {
				list.add(t);
			}
		}
		// return the new list
		return list;
	}

}
