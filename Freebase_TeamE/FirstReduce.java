/*
 * Filename: FirstReduce.java
 * Description: This class is for forming a direct relation between persons if they have indirect relation.
 * Author: Jay
 * 
 * Input-->
 * 	m.0jcx	[people.person.parents m.03gq3xh, education.academic.advisees  m.015418]	--> direct relations 	
 *  Not_Person+m.0hyl4r5	[people.person.spouse_s m.0jcx, people.person.spouse_s  01rywh] --> indirect relations
 *  
 * Output-->
 * 
 * Direct relations will be the same i.e., nothing will be modified.
 * m.0jcx	[people.person.parents m.03gq3xh, education.academic.advisees  m.015418]
 *  
 * Indirect relations will be modified as:
 * 	m.ojcx	people.person.spouse_s  m.01rywh
 *  m.01rywh people.person.spouse_s  m.0jcx
 * 
 * Method in this class
 *   reduce function---
 *   	This function will be called for each key value pair and make indirect relations as direct relations. 
 *   
 *   
 *   
 */




package Freebase_TeamE;

import java.io.IOException;
import java.util.ArrayList;
/*import java.util.HashMap;
import java.util.Map;
*/import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class FirstReduce
	extends Reducer<Text,Text,Text,Text>
{
	
	/*
	 * The algorithm for converting indirect relations to direct relations is:
	 * 
	 *   1) If key contains "Not_Person" then it is a indirect relation id.
	 *   1.1) The value contain all of person ids and their relationship. Personid and relationship will be one string. Store these all strings in one arrayList
	 *   1.2) If the arrayList has more than one element continue further else nothing. 
	 *   1.3) For each element in the array get personid and make it as key and remaining all elements will become value for this key.
	 *   1.4) Write the each key value pair to Context.  
	 * 	 2) If key does not contain "Not_Person" then just write input's key value pair to Context because it is already has direct relations.
	 */ 
	
	public void reduce(Text key,Iterable<Text> values,Context context) throws IOException,InterruptedException
	{
		//newKey and newValue
		Text newKey,newValue;
		
		//ArrayList to hold all values.
		ArrayList<String> relations;
		//StringTokenizer to divide personid and relation name for a single element in the array
		StringTokenizer relationPersonID;
		
		//If input key contains "Not_Person" then it is a indirect relation id, so do the processing
		if(key.toString().contains("Not_Person"))
		{
			relations = new ArrayList<String>();
			//Store each value in the key value pair in arraylist.
			for(Text value : values)
			{
				relations.add(value.toString());
			}
			
			//Each indirect relation has minimum of 2 persons. This if condition is useful if dataset has some bad records.
			if(relations.size() >=2)
			{
				//For each entry in the array
				for(int i=0;i<relations.size();i++)
				{
					//split the entry into key and relation
					relationPersonID = new StringTokenizer(relations.get(i));
					relationPersonID.nextToken();
					//get the person id from the above tokenizer and store that as newKey
					newKey = new Text(relationPersonID.nextToken().toString());
					
					//For remaining all entries in the array store them as value for the above newKey and write the key value pair into output
					for(int j=0;j<relations.size();j++)
					{
						//Except the above entry
						if(i!=j)
						{
							newValue = new Text(relations.get(j).toString());
							context.write(newKey,newValue);
						}
					}
				}
			}
		}
		
		//If input key does not contain "Not_person". That means key has direct relations. So just write the same input key and value pair to output
		else
		{
			for(Text value : values)
			{
				context.write(key, value);
			}
		}
	}
}
