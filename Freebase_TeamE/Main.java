package Freebase_TeamE;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.fs.Path;

/* Filename: Main.java
 * Author: Sakethram Karumuri
 * Description: Freebase is a database that is like wikipedia.  It is setup in a graph that connects all nodes together with different kinds of relationships.  
 * This program uses Freebase data to create a graph problem that finds the largest clique of people.  A clique is a pattern in a graph in which all
 * the nodes have edges that interconnect.  In this program the nodes are people and the edges are the relationships that they have with one another.
 * For example Luke Skywalker would have a child relationship with Darth Vader.  The largest clique in Freebase will show the largest circle of related people
 * who are famous enough to be a part of the Freebase database. 
 * 
 * Problem Statement:
 * Which domain (such as family, education, politics, etc.) has the most number of connected people? 
	Vertices: Persons
	Edges: Any relation between two persons is taken as an edge

 * Implementation approach:
 * This program is divided into four jobs and one separate program.
 * 
 * First Job: Takes in the raw Freebase data and pulls all the relationships between people out.  It then organizes these people
 * and relationships and creates links for the graph.  The end result will be a single person (person1 id) as a key and the relationship (with person2) + person2 id as the value
 * 
 * Second Job:  The main task of this job is to tackle the sub graph problem. As each node in the cluster will just have a sub graph, we need to have the adjacency list for each person in the adjacency list of a person.
 * This is done by replicating the data which is explained more clearly in the SecondReduce.java file.
 *  
 * Third Job:  This job goes through each of the different keys and creates the biggest possible clique with the information for each node.
 * 
 * Fourth Job: Takes each of the cliques created by the third job and finds the largest. The output will contain the mid's present in the clique.
 * Post-processing program: This program takes the final output with the clique information (in mid's) and converts the person ids to names and makes the output easier to read.
 * 
 * 
 * Authors:Ravikanth, Saketh, Jay, Brenda
 * 
 * Contributions:
 * Ravikanth: Main file (for post processing), fourth map, fourth reduce, script files, testing
 * Saketh: Main file (for Map Reduce), third map, third reduce, testing
 * Jay: first map, first reduce
 * Brenda: second map, second reduce, testing
 * 
 * Input:  The raw Freebase data.  Each line of this data has the format: node1 http address TAB relationship between the two nodes http address TAB node2 http address TAB .
 * Output: The output is a file with the maximal clique
 * 
 * 
 * - Input file format (N-Triples RDF):
	<subject>  <predicate>  <object> .
	- Each of them are seperated with tabs as the delimiter.
	Eg:
	<http://rdf.freebase.com/ns/m.0gl_1t7>	<http://rdf.freebase.com/ns/people.person.sibling_s>	<http://rdf.freebase.com/ns/m.0vvd9dd>	.

- Output file format (from 4th map reduce, not human readable):
	Clique size
	Person1 MID
	Person2 MID
	.
	.
	.
	Person1 MID-Person2 MID: Relation between them
	Person2 MID-Person3 MID: Relation between them
	Person3 MID-Person4 MID: Relation between them
	.
	.
	.

	Eg:
	Clique size: 3
	m.01rywh
	m.07wpwv
	m.0jcx
	m.01rywh-m.07wpwv:people.person.parents
	m.01rywh-m.0jcx:people.person.spouse_s
	m.07wpwv-m.01rywh:people.person.parents
	m.07wpwv-m.0jcx:people.person.children
	m.0jcx-m.01rywh:people.person.spouse_s
	m.0jcx-m.07wpwv:people.person.children

- Final output file format (human readable):
	Eg:
	[Clique size: 3] -->
 	Persons in this clique are:

	1)  Mileva Mari\u0107
	2)  Eduard Einstein
	3)  Albert Einstein

 	Relations between these persons are:--> 

 	{Mileva Mari\u0107,Eduard Einstein} --> [people.person.parents]
	{Mileva Mari\u0107,Albert Einstein} --> [people.person.spouse_s]
	{Eduard Einstein,Mileva Mari\u0107} --> [people.person.parents]
	{Eduard Einstein,Albert Einstein} --> [people.person.children]
	{Albert Einstein,Mileva Mari\u0107} --> [people.person.spouse_s]
	{Albert Einstein,Eduard Einstein} --> [people.person.children]

 */

public class Main {
	
	public static void main(String args[]) throws Exception
	{
		
		//declare variables for storing each jobs output
		String firstJobInput = args[0];
		String firstJobOutput = args[1]+"/firstJobOutput";
		String secondJobOutput = args[1]+"/secondJobOutput";
		String thirdJobOutput = args[1]+"/thirdJobOutput";
		String fourthJobOutput = args[1]+"/fourthJobOutput";
		
		//creates first job
		Configuration conf = new Configuration();
		Job job = new Job(conf,"FirstMapReduce");
		
		//sets the jar file
		job.setJarByClass(Main.class);
		
		//sets the first mapper and reducer to the first job
		job.setMapperClass(FirstMap.class);
		job.setReducerClass(FirstReduce.class);
		
		//compute the optimal number of reducer tasks and set them
		int numOfReduceTasksOne = ((Double)(0.95 * 16 * Integer.parseInt(job.getConfiguration().get("mapred.tasktracker.reduce.tasks.maximum","2")))).intValue();
		job.setNumReduceTasks(numOfReduceTasksOne);
		
		//set i/p and o/p formats for mapper and reducers
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		//set the i/p and o/p paths
		FileInputFormat.addInputPath(job, new Path(firstJobInput));
		FileOutputFormat.setOutputPath(job, new Path(firstJobOutput));
		
		//run the job and wait for its completion
		job.waitForCompletion(true);
		
		//sets up the second job with the second mapper and reducer
		Configuration conf2 = new Configuration();
		Job jobtwo = new Job(conf2,"SecondMapReduce");
		
		//sets the jar file
		jobtwo.setJarByClass(Main.class);
		
		//sets the second mapper and reducer to the second job
		jobtwo.setMapperClass(SecondMap.class);
		jobtwo.setReducerClass(SecondReduce.class);
		
		//compute the optimal number of reducer tasks and set them
		int numOfReduceTasksTwo = ((Double)(0.95 * 16 * Integer.parseInt(jobtwo.getConfiguration().get("mapred.tasktracker.reduce.tasks.maximum","2")))).intValue();
		jobtwo.setNumReduceTasks(numOfReduceTasksTwo);

		//set i/p and o/p formats for mapper and reducers
		jobtwo.setOutputKeyClass(Text.class);
		jobtwo.setOutputValueClass(Text.class);
		
		//set the i/p and o/p paths
		FileInputFormat.addInputPath(jobtwo, new Path(firstJobOutput));
		FileOutputFormat.setOutputPath(jobtwo, new Path(secondJobOutput));
		
		//run the job and wait for its completion
		jobtwo.waitForCompletion(true);

		//sets up the third job with the third mapper and reducer
		Configuration conf3 = new Configuration();
		Job jobthree = new Job(conf3,"ThirdMapReduce");
		
		//sets the jar file
		jobthree.setJarByClass(Main.class);
		
		//sets the third mapper and reducer to the third job
		jobthree.setMapperClass(ThirdMap.class);
		jobthree.setReducerClass(ThirdReduce.class);
		
		//compute the optimal number of reducer tasks and set them
		int numOfReduceTasksThree = ((Double)(0.95 * 16 * Integer.parseInt(jobthree.getConfiguration().get("mapred.tasktracker.reduce.tasks.maximum","2")))).intValue();
		jobthree.setNumReduceTasks(numOfReduceTasksThree);
		
		//set i/p and o/p formats for mapper and reducers
		jobthree.setOutputKeyClass(Text.class);
		jobthree.setOutputValueClass(Text.class);
		
		//set the i/p and o/p paths
		FileInputFormat.addInputPath(jobthree, new Path(secondJobOutput));
		FileOutputFormat.setOutputPath(jobthree, new Path(thirdJobOutput));
		
		//run the job and wait for its completion
		jobthree.waitForCompletion(true);

		//creates job 4 with the fourth mapper and reducer
		Configuration conf4 = new Configuration();
		Job jobfour = new Job(conf4,"FourthMapReduce");
		
		//sets the jar file
		jobfour.setJarByClass(Main.class);
		
		//sets the fourth mapper and reducer to the fourth job
		jobfour.setMapperClass(FourthMap.class);
		jobfour.setReducerClass(FourthReduce.class);
		
		//sets the number of reducer tasks to 1 to find the largest clique from the entire graph
		jobfour.setNumReduceTasks(1);

		//set i/p and o/p formats for mapper and reducers
		jobfour.setOutputKeyClass(Text.class);
		jobfour.setOutputValueClass(Text.class);
		
		//set the i/p and o/p paths
		FileInputFormat.addInputPath(jobfour, new Path(thirdJobOutput));
		FileOutputFormat.setOutputPath(jobfour, new Path(fourthJobOutput));
		
		//run the job and exit upon successful completion 
		System.exit(jobfour.waitForCompletion(true) ? 0:1);		

	}

}
