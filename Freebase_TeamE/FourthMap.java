/*
 * Filename: FourthMap.java
 * Description:
 * method in this Class:
 * 						map(Object key,Text value,Context context)		
 * 
 * Author: Ravikanth Repaka
 * 
 * This will read the output of ThirdReduce Job and will make a new key and value pair and writes the new key value pair to the Context.
 * 
 * 
 * The input for this map is max cliques from different reduce tasks. Each clique is one input line with nodes as tab separated along with entire relations without any delimeters.   
 * 
 * 	new key is entire input value.
 *  new value is number of number of nodes in the clique
 * 
 * Example:
 * 
 * Input->
 * lets suppose input clique has 10 MIDs then the input format is 
 * MID	MID	MID	MID	MID	MID	MID	MID	MID	MID	RelationsBetweenEachMID 
 * 
 * Output ->
 * 									key										|					value
 * -------------------------------------------------------------------------------------------------------------------
 * MID	MID	MID	MID	MID	MID	MID	MID	MID	MID	RelationsBetweenEachMID			|					10
 * 
 * 
 */





package Freebase_TeamE;


import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class FourthMap 
	extends  Mapper<Object,Text,Text,Text>
{
	//Function will be called for each line of input
	public void map(Object key,Text value,Context context) throws IOException,InterruptedException
	{
		//Convert the whole value into string
		String val = value.toString();
		//split the string based with tab delimiter
		String[] split = val.split("\t");
		//make the entire input value as key and number of node in the clique as value. Write the new key and new value pair to Context  
		context.write(new Text(val), new Text(Integer.toString(split.length-1)));
	}
}
