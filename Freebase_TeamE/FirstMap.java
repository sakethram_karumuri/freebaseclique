/*
 * Filename: FirstMap.java
 * Description: This is the first map in the Freebase program.  It takes the 25.8GB (308GB uncompressed) of raw data from Freebase and extracts the people.
 * If a line contains a person, then the person is stored as the key and the relationship and what it is related to is stored as the value.
 * Freebase has a few relationships that are not direct meaning that there is two steps to get to the person.  If this is the case
 * The key is the what the person is related to with a tag to indicate the indirect relationship for the reducer.
 * 
 * Author: Jay
 * 
 * Input:  Object one url tab relationship url tab object 2 url tab .
 * Output: Either
 *         Key: Not_PersonID Value: relationship tab personID
 *         Key: personID Value: relationship tab personID
 *         
 * Example: 
 * I/P:
 * <http://rdf.freebase.com/ns/m.0vvd9dd>	<http://rdf.freebase.com/ns/people.sibling_relationship.sibling>	<http://rdf.freebase.com/ns/m.03hpfv5>	.
 * O/P:
 * Key: m.0vvd9dd	Value: people.sibling_relationship.sibling	m.03hpfv5
 * 
 * 
 */


package Freebase_TeamE;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class FirstMap 
    extends  Mapper<Object,Text,Text,Text>
{
    
    public void map(Object key,Text value,Context context) throws IOException,InterruptedException
	{
    	//split the raw Freebase by the tabs into three different strings 
	    String inputString = value.toString();
	    String[] split = inputString.split("\t");	
	    //Some lines may be corrupted so a check is made to ensure that the line is in the correct Freebase format.
		if(split.length == 4)
		    {

			//put the seperate strings into variables
			String str1 = split[0];
			String str2 = split[1];
			String str3 = split[2];
			//Another check to make sure that the different strings are in Freebase format
			if(str1.startsWith("<http") &&  str2.startsWith("<http") && str3.startsWith("<http"))
			    {
				//get the id of the first and third string and the relationship in the second string
				str1 = str1.substring(str1.lastIndexOf("/")+1,str1.lastIndexOf(">"));
				str2 = str2.substring(str2.lastIndexOf("/")+1,str2.length()-1);
				str3 = str3.substring(str3.lastIndexOf("/")+1,str3.lastIndexOf(">"));
		    
				//check to see if the relationship is between people and to see if it is a direct relationship
				if(str2.contains("person.parents") || str2.contains("person.children") || str2.contains("artist.supporting_artists") || str2.contains("group_member.artists_supported") || str2.contains("influence_node.influenced_by") || str2.contains("influence_node.influenced") || str2.contains("us_vice_president.to_president") || str2.contains("us_president.vice_president"))
				    context.write(new Text(str1),new Text(str2+"\t"+str3));
				//check to see if the relationship is between people and to see if it is a indirect relationship
				else if(str2.contains("person.sibling_s") || str2.contains("person.spouse_s") || str2.contains("influence_node.peers") || str2.contains("celebrity_friends") || str2.contains("political_appointer.appointees") || str2.contains("politician.government_positions_held") || str2.contains("appointer.appointment_made") || str2.contains("appointee.position") || str2.contains("dedicatee.dedications") || str2.contains("celebrity.dated") || str2.contains("celebrity.friendship") || str2.contains("celebrity.supporter") || str2.contains("celebrity.celebrity_rivals"))
				    context.write(new Text("Not_Person"+str3),new Text(str2+"\t"+str1));

			    }
	}
}

}