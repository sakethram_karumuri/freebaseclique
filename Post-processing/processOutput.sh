#!/bin/bash -l

# Author : Ravikanth Repaka

set -e

# set the class name variable
CLASSNAME=Main

# set the classpath variables with httpclient jars
CLASSPATH=".:./commons-httpclient-3.1.jar:./org-apache-commons-codec.jar:./org-apache-commons-logging.jar"

# set the input file name
INPUTPATH=MapReduceOutput

# compile java file
javac -classpath $CLASSPATH $CLASSNAME.java

# run the program
java -classpath $CLASSPATH $CLASSNAME $INPUTPATH