
/* Filename: Main.java
 * 
   Description: This program is meant to be run on a home device not on Itasca.  This main file uses file readers and writers to turn the output of the fourth ruduce of the
 * Freebase program into human readable output.  The input for this program should be the file that is outputed by the fourth and final job of the Freebase program.
 * The input will have the size of the largest clique found by the Freebase program, the mIDs for all of the people that are a part of the clique, and the relationships
 * between all of the people in the clique.  This program takes the mIDs and retrieves the name of the people from Freebase using the Http client.  It then outputs a new
 * file that has the clique size, name of all the people in the clique, and the relationships between all of the people in the clique in english.
 * 
   Author: Ravikanth Repaka
 */


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;


public class Main 
{
	//Creates a hash to store the names with their mIDs so they can be used for the relationships
	static Map<String,String> MIDNames = new HashMap<String,String>();
	
	//Creates the beginning of the url that will enable the program to retrieve the names of the people in the clique
	static String url = "http://rdf.freebase.com/ns/";

	static HttpClient client = new HttpClient();

	//main method where input file is read and processed then written to the output file
	public static void main(String args[]) throws IOException
	{
		//create a file reader
		BufferedReader br;
		FileReader fr;


		if(args.length != 1)
		{
			System.out.println("Please enter input path file\n");
			System.exit(0);
		}

		br = new BufferedReader(new FileReader(args[0])); 
		
		//create a file for the output
		File file = new File("Freebase_MaxClique.txt");
		
		//if file doesn't exisit creae a new file to avoid any errors
		if(!file.exists())
		{
			file.createNewFile();
		}

		//create a file writer for the output file
		FileWriter fw = new FileWriter(file.getAbsoluteFile());

		BufferedWriter bw = new BufferedWriter(fw);

		String line;
		String lineToWrite="";
		String MID;
		//for outputing the people involved with the clique
		int number=1;
		//indicates the first line of the relationships
		boolean flag = true;

		//while there are still lines in the input file
		while((line = br.readLine()) != null)
		{
			//if the line doesn't contain the word Clique
			if(!line.contains("Clique"))
			{
				//if the line doesn't contain a colon
				if(!line.contains(":"))
				{
					//delete all spaces
					MID = line.replaceAll("\\s+","");
					//System.out.println(MID);
					lineToWrite = number++ + ")  ";
					//if the mID is in the hash table then use the name from the table
					if(MIDNames.containsKey(MID))
						lineToWrite += MIDNames.get(MID);					
					//otherwise lookup the name in Freebase using getName method
					else
						lineToWrite += getName(MID);
				}
				//the line contains a colon
				else
				{
					//if it is the firt line of the relationships then output this message in the file and set the flag to false
					if(flag == true)
					{
						lineToWrite = "\n Relations between these persons are:--> \n\n ";
						flag = false;
					}
					//split the line by the colon
					StringTokenizer lineTokenizer = new StringTokenizer(line,":-");
					lineToWrite += "{";
					//for both people on the line either find their names in the hash table or look them up in Freebase using the getName method
					//At this stage the program should not have to use the function getName and all the names should be in the hash table
					for(int i=0;i<2;i++)
					{
						//get the person from the line
						MID = lineTokenizer.nextToken();
						//find the name in the hash table and append it to the line to write
						if(MIDNames.containsKey(MID))
							lineToWrite += MIDNames.get(MID);					
						//find th name in Freebase using getName
						else
							lineToWrite += getName(MID);
						//put a coma in between the first and second person
						if(i==0)
							lineToWrite += ",";
					}
					lineToWrite += "}" + " --> [" + lineTokenizer.nextToken().toString()+"]";
				}
			}
			//Clique is in the line so write the number of people in the clique and make sure number is set to one and flag is true
			else
			{
				lineToWrite = "[" + line + "] -->\n Persons in this clique are:\n";
				number =1;
				flag = true;
			}
			

			lineToWrite += "\n";
			//write the line to the file
			bw.write(lineToWrite);
			//clears history
			lineToWrite = "";
		}
		//close the two files
		br.close();
		bw.close();
	}

	//this method is where the program connects to Freebase to retrieve the names that correspond to the mIDs.
	public static String getName(String MID)
	{
		GetMethod method = new GetMethod(url+MID);
		// Provide custom retry handler is necessary
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
				new DefaultHttpMethodRetryHandler(3, false));

		try {
				// Execute the method.
				int statusCode = client.executeMethod(method);
				if (statusCode != HttpStatus.SC_OK) {
					System.err.println("Method failed: " + method.getStatusLine());
				}
	
				// Read the response body.
				InputStream responseBody = method.getResponseBodyAsStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(responseBody));
				// Deal with the response.
				// Use caution: ensure correct character encoding and is not binary data
				String line;
				//System.out.println("Inside for");
				String name=MID; 
				while((line = reader.readLine()) != null)
				{
					//If the line contains the name then extract it into name
					//System.out.println(line);
					if(line.contains("rdfs:label") && line.contains("@en")) 
						name = line.substring(line.indexOf("\"")+1, line.lastIndexOf("\""));
				}
	
				responseBody.close();
				reader.close();
				
				MIDNames.put(MID, name);
				//System.out.println(MID+" "+name);
				return name;		      
		} 
		//catch any errors with the Http client
		catch (HttpException e) {
			System.err.println("Fatal protocol violation: " + e.getMessage());
			e.printStackTrace();
			return MID;
		}
		//catch any I/O errors
		catch (IOException e) {
			System.err.println("Fatal transport error: " + e.getMessage());
			e.printStackTrace();
			return MID;
		}
		finally {
			// Release the connection.
			method.releaseConnection();			
		}  

	}
}


